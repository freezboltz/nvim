local VO = vim.opt
local VG = vim.g
VO.guicursor = ""
VO.nu = true
VO.relativenumber = true
VO.tabstop = 4
VO.softtabstop = 4
VO.shiftwidth = 4
VO.expandtab = true
VO.smartindent = true
VO.wrap = false

VO.hlsearch = false
VO.incsearch = true

VO.termguicolors = true

VO.scrolloff = 8
VO.signcolumn = "yes"
VO.updatetime = 50

VO.colorcolumn = "80"
VG.mapleader = " "

-- Autocommands
-- clean trailing whitespace and new line from <EOF>.
local clean = vim.api.nvim_create_augroup("Cleaner", {clear = true})
vim.api.nvim_create_autocmd("BufWritePre", {pattern = "*", command = [[%s/\s\+$//e]], group = clean, desc = "Remove trailing white space"})
vim.api.nvim_create_autocmd("BufWritePre", {pattern = "*", command = [[%s/\n\+\%$//e]], group = clean, desc = "Remove trailing new line"})
