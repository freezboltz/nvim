vim.keymap.set("n", "<leader>ex", vim.cmd.Ex, { desc = "[ex]plorer open netrw vim file explorer" })

-- Just ignore Q
vim.keymap.set("n", "Q", "<nop>")
--delete to void register and paste the latest from register
vim.keymap.set("n", "<leader>p", "\"_dP")

vim.keymap.set("n", "[c", function()
	require("treesitter-context").go_to_context()
end, { silent = true, desc = "jumping to context" })
