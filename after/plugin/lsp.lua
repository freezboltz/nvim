local lsp = require("lsp-zero")
local lspconf = require("mason-lspconfig")

lsp.preset("recommended")

lspconf.setup({
	ensure_installed = { "lua_ls", "pyright", "tailwindcss", "tsserver", "jsonls", "eslint" },
})

-- Fix Undefined global 'vim'
lsp.configure("lua_ls", {
	settings = {
		Lua = {
			diagnostics = {
				globals = { "vim", "awesome", "root", "client" },
			},
		},
	},
})

-- Disable diagnostic virtual_text
vim.diagnostic.config({
	virtual_text = false,
})

local cmp = require("cmp")
local cmp_select = { behavior = cmp.SelectBehavior.Select }
local cmp_mappings = lsp.defaults.cmp_mappings({
	["<C-p>"] = cmp.mapping.select_prev_item(cmp_select),
	["<C-n>"] = cmp.mapping.select_next_item(cmp_select),
	["<C-y>"] = cmp.mapping.confirm({ select = true }),
	["<C-Space>"] = cmp.mapping.complete(),
})

cmp_mappings["<Tab>"] = nil
cmp_mappings["<S-Tab>"] = nil

lsp.setup_nvim_cmp({
	mapping = cmp_mappings,
})

lsp.on_attach(function(client, bufnr)
	local opts = { buffer = bufnr, remap = false }
	local v = vim
	local km = v.keymap.set

	km("n", "gd", function()
		v.lsp.buf.definition()
	end, opts)
	km("n", "K", function()
		v.lsp.buf.hover()
	end, opts)
	km("n", "ca", function()
		v.lsp.buf.code_action()
	end, opts)
	km("n", "gl", function()
		v.diagnostic.open_float()
	end, opts)
	km("n", "[d", function()
		v.diagnostic.goto_next()
	end, opts)
	km("n", "]d", function()
		v.diagnostic.goto_prev()
	end, opts)
	km("n", "gr", function()
		v.lsp.buf.references()
	end, opts)
	km("i", "<C-h>", function()
		v.lsp.buf.signature_help()
	end, opts)
end)
lsp.setup()
